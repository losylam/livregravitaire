/**
 * Livre Gravitaire
 * 
 * @author Laurent Malys (lo@crans.org)
 * @version 0.3.0 29/05/2018
 */


#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  ofSetFrameRate(60);

  ofxAccelerometer.setup();
  //font.loadFont("verdana.ttf",24);
  draw_accel = false;

  bd.setup();
  screen_width = ofGetWidth();
  screen_height = ofGetHeight();
  if (screen_width < screen_height) {
    screen_width = ofGetHeight();
    screen_height = ofGetWidth();
  }
  b_update = true;

  ofSetVerticalSync(true);
}

//--------------------------------------------------------------
void ofApp::update(){
  accel = ofxAccelerometer.getForce();
  messages[0] = "g(x) = " + ofToString(accel.x,2);
  messages[1] = "g(y) = " + ofToString(accel.y,2);
  messages[2] = "g(z) = " + ofToString(accel.z,2);
  normAccel = accel.getNormalized();
  ofLogNotice("gx:" + ofToString(normAccel.x) + " gy:" + ofToString(normAccel.y));


  if (b_update){
    //float g_x = (mouseX * 2 - screen_width)/screen_width;
    //float g_y = (mouseY * 2 - screen_height)/screen_height;
    ofLogNotice("framerate:" + ofToString(ofGetFrameRate()));
    bd.update(normAccel.x,normAccel.y);
  }
}

//--------------------------------------------------------------
void ofApp::draw(){
  ofBackground(50,50,50);
  //img_test.draw(0,0);
  bd.draw();

  if (draw_accel){
    ofPushStyle();
    // ofSetColor(255,255,0);
    // font.drawString(messages[0],10,font.stringHeight(messages[0])+20);
    // ofSetColor(255,0,255);
    // font.drawString(messages[1],10,(font.stringHeight(messages[0])+20)*2);
    // ofSetColor(0,255,255);
    // font.drawString(messages[2],10,(font.stringHeight(messages[0])+20)*3);
    
    ofPushMatrix();
    ofTranslate(ofGetWidth()/2, ofGetHeight()/2);

    ofSetColor(255,255,0);
    ofLine(0,0,normAccel.x*ofGetWidth()/2,0);
    ofSetColor(255,0,255);
    ofLine(0,0,0,-normAccel.y*ofGetHeight()/2);
    // we don't draw z as the perspective might be confusing
    // but it's approximately one when the device is still and parallel
    // to the ground
    ofPopMatrix();
    ofPopStyle();
  }
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){ 
	
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){ 
	
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::touchDown(int x, int y, int id){
  bd.slow(true);
  bd.mousePressedOnMap();
}

//--------------------------------------------------------------
void ofApp::touchMoved(int x, int y, int id){

}

//--------------------------------------------------------------
void ofApp::touchUp(int x, int y, int id){
  bd.slow(false);
}

//--------------------------------------------------------------
void ofApp::touchDoubleTap(int x, int y, int id){

}

//--------------------------------------------------------------
void ofApp::touchCancelled(int x, int y, int id){

}

//--------------------------------------------------------------
void ofApp::swipe(ofxAndroidSwipeDir swipeDir, int id){
  bd.showMap();
}

//--------------------------------------------------------------
void ofApp::pause(){

}

//--------------------------------------------------------------
void ofApp::stop(){

}

//--------------------------------------------------------------
void ofApp::resume(){

}

//--------------------------------------------------------------
void ofApp::reloadTextures(){

}

//--------------------------------------------------------------
bool ofApp::backPressed(){
	return false;
}

//--------------------------------------------------------------
void ofApp::okPressed(){

}

//--------------------------------------------------------------
void ofApp::cancelPressed(){

}
