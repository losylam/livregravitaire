/**
 * Livre Gravitaire
 * 
 * @author Laurent Malys (lo@crans.org)
 * @version 0.3.0 29/05/2018
 */


#include "bdGravitaire.h"

BdGravitaire::BdGravitaire(){
  
}

void BdGravitaire::setup(){

  mode_debug = true;

  mode_slow = false;
  
  screen_height = ofGetScreenHeight();
  screen_width = ofGetScreenWidth();

  show_map = false;
  map_anim_step = 0;
  map_anim_nsteps = 30;
  
  map_width = 18710;
  map_height = 14843;

  float ratio_x = map_width / (float)screen_width;
  float ratio_y = map_height / (float)screen_height;

  if (ratio_x > ratio_y){
    map_ratio = ratio_x;
  }else{
    map_ratio = ratio_y;
  }
  
  map_w_ratio = map_width/map_ratio;
  map_h_ratio = map_height/map_ratio;
  
  source_width = 468.296;
  source_height = 307.216;

  angle_forward = 50;
  angle_backward = 30;

  accel = 0.2;

  vit = 0;

  g_x = 0;
  g_y = 0;

  min_y = 0;
  max_y = -1;

  min_x = -0.5;
  max_x = 0.5;

  bubble_size = 50;
  bubble_margin = 10;
  ofSetCircleResolution(100);
  
  int nx_img = 19;
  //  int ny_img = 16;


  
  string parts_dir = "images/parts";
  ofDirectory dir(parts_dir);
  dir.allowExt("png");
  int n_img = dir.listDir();
  
  for (int i = 0; i < n_img; i++){
    string path  = dir.getPath(i);
    int num = ofToInt(path.substr(19,3));
    int pos_x = num%nx_img;
    int pos_y = num/nx_img;
    BdImage bd_img;
    bd_img.path = path;
    bd_img.pos_x = pos_x * 1000;
    bd_img.pos_y = pos_y * 1000;
    bd_img.num = num;
    //bd_img.img.loadImage(path);
    bd_img.is_loaded = false;
    imgs.push_back(bd_img);

    ofLogNotice(ofToString(ofToInt(path.substr(19,2)))
		+ " " + ofToString(pos_x)
		+ " " + ofToString(pos_y));    
  }

  points.push_back(ofPoint(7072.2114, 461.35709)); // z000
  points.push_back(ofPoint(9311.4395, 461.35709)); // rect10080
  points.push_back(ofPoint(9311.4395, 764.57288)); // rect9172
  points.push_back(ofPoint(9775.7354, 764.57288)); // rect9560
  points.push_back(ofPoint(9775.7354, 1067.7892)); // rect9546
  points.push_back(ofPoint(9311.4395, 1067.7892)); // rect9562
  points.push_back(ofPoint(9311.4395, 1370.9972)); // rect9548
  points.push_back(ofPoint(9775.7354, 1370.9972)); // rect9564
  points.push_back(ofPoint(9775.7354, 1674.213)); // rect9550
  points.push_back(ofPoint(9311.4395, 1674.2133)); // rect9566
  points.push_back(ofPoint(9311.4395, 1977.4291)); // rect9552
  points.push_back(ofPoint(9775.7354, 1977.4291)); // rect9568
  points.push_back(ofPoint(9775.7354, 2280.6453)); // rect9554
  points.push_back(ofPoint(8847.1436, 2279.1892)); // rect9570
  points.push_back(ofPoint(8847.1436, 4071.6853)); // rect9576
  points.push_back(ofPoint(9855.7354, 4072.4773)); // rect9572
  points.push_back(ofPoint(9855.7354, 3769.2773)); // rect9556
  points.push_back(ofPoint(10320.031, 3769.2773)); // rect9558
  points.push_back(ofPoint(10320.031, 171.21402)); // rect9174
  points.push_back(ofPoint(14650.633, 171.21402)); // rect9176
  points.push_back(ofPoint(14650.9, 474.43082)); // rect9178
  points.push_back(ofPoint(15035.197, 697.64661)); // rect9180
  points.push_back(ofPoint(14650.9, 920.86243)); // rect9182
  points.push_back(ofPoint(15035.197, 1146.1027)); // rect9184
  points.push_back(ofPoint(14650.9, 1369.3182)); // rect9186
  points.push_back(ofPoint(15035.197, 1592.5343)); // rect9188
  points.push_back(ofPoint(14650.9, 1815.7501)); // rect9190
  points.push_back(ofPoint(15035.197, 2038.9662)); // rect10790
  points.push_back(ofPoint(14650.9, 2262.1904)); // rect10792
  points.push_back(ofPoint(15035.197, 2485.4065)); // rect10796
  points.push_back(ofPoint(14650.9, 2708.6223)); // rect10794
  points.push_back(ofPoint(15035.197, 2931.8464)); // rect9200
  points.push_back(ofPoint(14650.9, 3155.0623)); // rect10139
  points.push_back(ofPoint(15035.197, 3378.2864)); // rect10142
  points.push_back(ofPoint(14650.9, 3601.5022)); // rect11651
  points.push_back(ofPoint(15035.197, 3824.7261)); // rect11653
  points.push_back(ofPoint(14652.727, 4071.2903)); // rect10172
  points.push_back(ofPoint(14652.727, 4374.5059)); // rect9222
  points.push_back(ofPoint(14188.432, 4374.5059)); // rect6158
  points.push_back(ofPoint(14188.432, 4677.7217)); // rect9206
  points.push_back(ofPoint(13724.135, 4677.7217)); // rect9208
  points.push_back(ofPoint(13724.135, 4980.9375)); // rect11699
  points.push_back(ofPoint(13259.84, 4980.9375)); // rect11703
  points.push_back(ofPoint(13259.84, 5284.1538)); // rect6166
  points.push_back(ofPoint(12795.543, 5284.1538)); // rect6170
  points.push_back(ofPoint(12795.543, 6030.1719)); // rect6174
  points.push_back(ofPoint(13293.695, 6030.1719)); // rect11715
  points.push_back(ofPoint(13293.695, 6342.1372)); // rect9226
  points.push_back(ofPoint(15708.812, 6336.8115)); // rect9220
  points.push_back(ofPoint(15708.812, 6640.0278)); // rect6154
  points.push_back(ofPoint(16493.061, 6640.0278)); // rect11906
  points.push_back(ofPoint(16493.061, 6336.8115)); // rect9526
  points.push_back(ofPoint(16957.355, 6336.8115)); // rect6167
  points.push_back(ofPoint(16957.355, 6033.5957)); // rect6169
  points.push_back(ofPoint(18099.492, 6033.5957)); // rect6198
  points.push_back(ofPoint(18099.492, 6752.0166)); // rect6203
  points.push_back(ofPoint(17635.195, 6752.0166)); // rect6205
  points.push_back(ofPoint(17635.195, 7367.7734)); // rect9528
  points.push_back(ofPoint(17170.898, 7367.7734)); // rect6284
  points.push_back(ofPoint(17170.898, 7733.521)); // rect6336
  points.push_back(ofPoint(12005.38, 7733.521)); // rect6346
  points.push_back(ofPoint(12005.38, 8316.7021)); // rect6368
  points.push_back(ofPoint(11514.604, 8316.7021)); // rect6364
  points.push_back(ofPoint(11512.541, 7635.3779)); // rect6372
  points.push_back(ofPoint(8348.3193, 7635.3779)); // rect6382
  points.push_back(ofPoint(8348.3193, 8157.2588)); // rect9234
  points.push_back(ofPoint(8812.624, 8157.2588)); // rect9256
  points.push_back(ofPoint(8812.624, 8460.4824)); // rect9236
  points.push_back(ofPoint(9276.9199, 8460.4824)); // rect9258
  points.push_back(ofPoint(9276.9277, 8157.2588)); // rect9238
  points.push_back(ofPoint(9741.2246, 8157.2588)); // rect9516
  points.push_back(ofPoint(9741.2158, 8790.4639)); // rect9242
  points.push_back(ofPoint(9276.9121, 8790.4639)); // rect9260
  points.push_back(ofPoint(8892.6152, 9013.6797)); // rect9244
  points.push_back(ofPoint(8510.9121, 8790.4639)); // rect9262
  points.push_back(ofPoint(8129.1909, 8707.0391)); // rect6221
  points.push_back(ofPoint(7747.4878, 8790.4639)); // rect6231
  points.push_back(ofPoint(7365.7671, 9013.6797)); // rect6233
  points.push_back(ofPoint(6973.7109, 8790.4639)); // rect6245
  points.push_back(ofPoint(6591.999, 9013.6797)); // rect6371
  points.push_back(ofPoint(6210.2866, 8790.4639)); // rect6259
  points.push_back(ofPoint(5629.1494, 9209.0254)); // rect6318
  points.push_back(ofPoint(5247.437, 8985.8096)); // rect6324
  points.push_back(ofPoint(5711.7329, 8634.5928)); // rect6325
  points.push_back(ofPoint(5330.0298, 8411.373)); // rect6347
  points.push_back(ofPoint(4472.8335, 8675.1709)); // rect6329
  points.push_back(ofPoint(4472.8335, 7502.7656)); // rect6448
  points.push_back(ofPoint(5477.1294, 7502.7656)); // rect6650
  points.push_back(ofPoint(5477.1294, 7171.4126)); // rect9290
  points.push_back(ofPoint(5941.4331, 7171.4126)); // rect6654
  points.push_back(ofPoint(5941.4331, 6868.1968)); // rect6670
  points.push_back(ofPoint(6945.4424, 6868.1968)); // rect6682
  points.push_back(ofPoint(6945.4424, 7171.4126)); // rect6652
  points.push_back(ofPoint(7409.7466, 7171.4126)); // rect6688
  points.push_back(ofPoint(7409.7466, 6744.082)); // rect6708
  points.push_back(ofPoint(7874.0503, 6744.082)); // rect6686
  points.push_back(ofPoint(7874.0503, 6440.8662)); // rect6844
  points.push_back(ofPoint(7409.7466, 6440.8662)); // rect6972
  points.push_back(ofPoint(7409.7466, 6137.6499)); // rect6976
  points.push_back(ofPoint(8231.2422, 6137.6499)); // rect9316
  points.push_back(ofPoint(8230.2881, 4387.5537)); // rect9512
  points.push_back(ofPoint(7719.9155, 4384.5513)); // rect9322
  points.push_back(ofPoint(7719.9155, 5513.7554)); // rect9334
  points.push_back(ofPoint(7204.7686, 5513.7554)); // rect7178
  points.push_back(ofPoint(7204.7686, 5115.3276)); // rect7489
  points.push_back(ofPoint(6740.4648, 5115.3276)); // rect7505
  points.push_back(ofPoint(6740.4648, 4812.1118)); // rect7653
  points.push_back(ofPoint(6275.2173, 4810.5342)); // rect7890
  points.push_back(ofPoint(6275.2173, 4507.3179)); // rect7892
  points.push_back(ofPoint(5810.9136, 4507.3179)); // rect6938
  points.push_back(ofPoint(5430.7847, 4730.5342)); // rect6940
  points.push_back(ofPoint(5049.0728, 4507.3179)); // rect6942
  points.push_back(ofPoint(4667.3608, 4730.5342)); // rect7462
  points.push_back(ofPoint(4285.6489, 4507.3179)); // rect7989
  points.push_back(ofPoint(3903.937, 4730.5342)); // rect8499
  points.push_back(ofPoint(3522.2251, 4507.3179)); // rect7176
  points.push_back(ofPoint(3057.9209, 4810.5342)); // rect6608
  points.push_back(ofPoint(3439.6328, 5033.75)); // rect6610
  points.push_back(ofPoint(3057.9209, 5256.9658)); // rect6663
  points.push_back(ofPoint(3439.6328, 5480.1821)); // rect6665
  points.push_back(ofPoint(3057.9209, 5703.3979)); // rect6753
  points.push_back(ofPoint(2593.6167, 5703.3979)); // rect6667
  points.push_back(ofPoint(2593.6167, 6006.6143)); // rect6854
  points.push_back(ofPoint(398.28271, 6006.6143)); // rect9356
  points.push_back(ofPoint(398.28271, 10451.909)); // rect6875
  points.push_back(ofPoint(1026.6167, 10451.909)); // rect6877
  points.push_back(ofPoint(1026.6167, 10109.825)); // rect6879
  points.push_back(ofPoint(1490.9209, 10109.826)); // rect6882
  points.push_back(ofPoint(1490.9209, 9806.6104)); // rect6943
  points.push_back(ofPoint(1955.2251, 9806.6104)); // rect6947
  points.push_back(ofPoint(1955.2251, 10109.826)); // rect6951
  points.push_back(ofPoint(2883.8335, 10109.826)); // rect6959
  points.push_back(ofPoint(2883.8335, 9806.6104)); // rect7001
  points.push_back(ofPoint(2419.5293, 9806.6104)); // rect7003
  points.push_back(ofPoint(2419.5293, 9503.3945)); // rect7013
  points.push_back(ofPoint(2883.8335, 9503.3945)); // rect6785
  points.push_back(ofPoint(2883.8335, 9200.1787)); // rect6787
  points.push_back(ofPoint(3348.1377, 9200.1787)); // rect6795
  points.push_back(ofPoint(3348.1377, 10022.401)); // rect6810
  points.push_back(ofPoint(3812.4419, 10022.401)); // rect6812
  points.push_back(ofPoint(3812.4419, 10325.616)); // rect6826
  points.push_back(ofPoint(7426.7295, 10321.284)); // rect6830
  points.push_back(ofPoint(7426.7295, 10624.499)); // rect7004
  points.push_back(ofPoint(7891.0332, 10624.499)); // rect7006
  points.push_back(ofPoint(7891.0332, 10927.716)); // rect7036
  points.push_back(ofPoint(7426.7295, 10927.716)); // rect7058
  points.push_back(ofPoint(7426.7295, 11230.931)); // rect6771
  points.push_back(ofPoint(6962.4253, 11230.931)); // rect6773
  points.push_back(ofPoint(6962.4253, 11534.147)); // rect6843
  points.push_back(ofPoint(6238.1216, 11534.147)); // rect6883
  points.push_back(ofPoint(6238.1216, 11230.931)); // rect6950
  points.push_back(ofPoint(5339.1216, 11230.931)); // rect6967
  points.push_back(ofPoint(5339.1216, 12850.182)); // rect6969
  points.push_back(ofPoint(5803.4253, 12850.182)); // rect6971
  points.push_back(ofPoint(5803.4253, 13153.397)); // rect6973
  points.push_back(ofPoint(6323.4253, 13153.397)); // rect8583
  points.push_back(ofPoint(6323.4253, 12805.063)); // rect8585
  points.push_back(ofPoint(6954.5366, 12805.063)); // rect8595
  points.push_back(ofPoint(6954.5366, 13540.079)); // rect8597
  points.push_back(ofPoint(7505.1938, 13540.079)); // rect8599
  points.push_back(ofPoint(7505.1938, 12501.849)); // rect8601
  points.push_back(ofPoint(7040.8901, 12501.849)); // rect8603
  points.push_back(ofPoint(7040.8901, 12198.632)); // rect9428
  points.push_back(ofPoint(8241.7842, 12198.632)); // rect8605
  points.push_back(ofPoint(8241.7842, 12501.849)); // rect9474
  points.push_back(ofPoint(8589.8252, 12719.5)); // rect7416
  points.push_back(ofPoint(8265.6348, 12992.014)); // rect7420
  points.push_back(ofPoint(8580.9912, 13264.754)); // rect7424
  points.push_back(ofPoint(8265.6348, 13553.169)); // rect7428
  points.push_back(ofPoint(8580.9912, 13828.559)); // rect7432
  points.push_back(ofPoint(8580.9912, 14167.61)); // rect7138
  points.push_back(ofPoint(11095.961, 14167.61)); // rect7837
  points.push_back(ofPoint(11095.961, 13020.802)); // rect7839
  points.push_back(ofPoint(9464.4434, 13020.802)); // rect7841
  points.push_back(ofPoint(9464.4434, 13852.8)); // rect7843
  points.push_back(ofPoint(10666.22, 13852.8)); // rect7845
  points.push_back(ofPoint(10666.22, 13330.614)); // rect7847
  points.push_back(ofPoint(9874.1963, 13330.614)); // rect7849
  points.push_back(ofPoint(9874.1963, 13585.461)); // rect7851
  points.push_back(ofPoint(10201.916, 13585.461)); // rect7853

  next_i_pos = -1;  
  pos = ofPoint(-7072.2114, -461.35709);
  //pos = ofPoint(0,0);
  //scale = (float)ofGetScreenWidth() / (float)source_width;
  scale = (float)ofGetScreenHeight() / (float)source_height;
  
  //ofLogNotice() << "carto_width:" << carto_width << " scale:" << scale << endl;
  w_mask = (ofGetScreenWidth() -(source_width * scale)) /2;

  ofLogNotice() << "w_mask " << w_mask << endl;
  // points.push_back(ofPoint(500, img.height-2000));
  // points.push_back(ofPoint(700, img.height-2000));
  // points.push_back(ofPoint(676, img.height-1862));
  // points.push_back(ofPoint(500, img.height-1844));//4
  // points.push_back(ofPoint(500, img.height-1700));
  // points.push_back(ofPoint(675, img.height-1690));
  // points.push_back(ofPoint(670, img.height-1550));
  // points.push_back(ofPoint(495, img.height-1530));//8
  // points.push_back(ofPoint(497, img.height-1393));
  // points.push_back(ofPoint(695, img.height-1373));
  // points.push_back(ofPoint(732, img.height-1235));
  // points.push_back(ofPoint(255, img.height-1216));//12
  // points.push_back(ofPoint(191, img.height-302));
  // points.push_back(ofPoint(735, img.height-285));

  is_moving_away = -1;
  is_on_point = 0;
  i_source = 0;
  i_last_source = 0;
  i_target = 1;
  source = points[0];
  target = points[1];

  current_direction = points[1] - points[0];
  current_direction.normalize();

  black_screen = 0;
  idle = false;
  idle_duration = 2;
  idle_out = false;
  idle_out_duration = 0.5;
}

void BdGravitaire::loadImagesDyn(){
  for (size_t i = 0; i < imgs.size() ; i++){
    if ((abs(imgs[i].pos_x + pos.x) < 2500) and (abs(imgs[i].pos_y + pos.y) < 2500)){
      if (imgs[i].is_loaded == false){
	ofLogNotice("load img: "+ofToString(i));
	loader.loadFromDisk(imgs[i].img, imgs[i].path);
	//	imgs[i].img.loadImage(imgs[i].path);
	imgs[i].is_loaded = true;
      }
    }else{
      if (imgs[i].is_loaded == true){
	ofLogNotice("clear img: "+ofToString(i));
	imgs[i].img.clear();
	imgs[i].is_loaded = false;
      }
    }
  }
}

int BdGravitaire::isOnPoint(){
  int out;
  if ((-pos - source).dot(current_direction) > (target - source).dot(current_direction)){
    out = i_target;
  } else if ((-pos - source).dot(current_direction) < 0){
    out = i_source;
  } else{
    out =  -1;
  }
  //ofLogNotice("pos " + ofToString((-pos - source).dot(current_direction))
  //	      + " target " + ofToString((target - source).dot(current_direction)));

  return out;
}


void BdGravitaire::moveForward(ofVec2f direction, int i){
  source = points[i];
  target = points[i+1];
  i_source = i;
  i_target = i+1;
  current_direction = (points[i+1] - points[i]);
  current_direction.normalize();
  //pos -= current_direction * 2 + current_direction * direction.dot(current_direction);
  is_on_point = -1;
}

void BdGravitaire::moveBackward(ofVec2f direction, int i){
  source = points[i];
  target = points[i-1];
  i_source = i;
  i_target = i-1;
  current_direction = points[i-1] - points[i];
  current_direction.normalize();
  //pos -=  current_direction * direction.dot(current_direction);
  is_on_point = -1;
}

void BdGravitaire::slow(bool is_slow){
  mode_slow = is_slow;
}

void BdGravitaire::update(float x, float y){
  //ofLogNotice() << "x:" << pos.x << " y:" << pos.y << endl;
  y = max(max_y, y);
  y = min(min_y, y);
  y = ofMap(y, min_y, max_y, 1, -1);

  x = max(min_x, x);
  x = min(max_x, x);
  x = ofMap(x, min_x, max_x, 1, -1);

  //  for (size_t i=0; i<points.size(); i++){
    //    if ((points[i].distance(-pos) < 3) and (is_moving_away <0)){
  //  if (is_moving_away <0){

  g_x = x;
  g_y = y;

  if (!show_map && !idle_out){
    updatePos();
  }
}

void BdGravitaire::updatePos(){

  ofVec2f direction(-g_x, -g_y);

  if (next_i_pos >=0){
    pos = -points[next_i_pos];
    current_direction = points[next_i_pos+1] - points[next_i_pos];
    current_direction.normalize();
    i_source = next_i_pos;
    i_target = next_i_pos + 1;
    source = points[next_i_pos];
    target = points[next_i_pos + 1];
    next_i_pos = -1;
    idle = true;
    idle_start = ofGetElapsedTimef();
  }
  
  is_on_point = isOnPoint();

  i_last_source = max(is_on_point, i_last_source);
  
  if (is_on_point>=0){
    pos = -points[is_on_point];
    vit = 0;
    if (is_on_point == 0){
      if (direction.isAligned(points[is_on_point+1] - points[is_on_point], angle_forward)){
	moveForward(direction, is_on_point);
      }
    }else if (is_on_point == (int)points.size()-1){
      // if (direction.isAligned(points[is_on_point-1] - points[is_on_point], angle_backward)){
      // 	moveBackward(direction, is_on_point);
      // }
      is_moving_away = -1;
      is_on_point = 0;
      i_source = 0;
      i_target = 1;
      source = points[0];
      target = points[1];

      pos = ofPoint(-1089, -528);

      current_direction = points[1] - points[0];
      current_direction.normalize();

      idle = true;
      idle_start = ofGetElapsedTimef();
    }else if (is_on_point >0){
      if (direction.isAligned(points[is_on_point+1] - points[is_on_point], angle_forward)){
	moveForward(direction, is_on_point);
      }else if(direction.isAligned(points[is_on_point-1] - points[is_on_point], angle_backward)){
	moveBackward(direction, is_on_point);
      }
    }
  }
    //}
  //}
  
  // if ((is_moving_away >= 0) and (points[is_moving_away].distance(-pos) > 4)){
  //   is_moving_away = -1;
  // }

  if (idle){
    float idle_time = ofGetElapsedTimef() - idle_start;
    if (idle_time < idle_duration){
      black_screen = ofMap(idle_time, 0, idle_duration, 255, 0);
    }else{
      idle = false;
      black_screen = 0;
    }
  }else{
    if (is_on_point<0){
      float cur_vit = vit;
      if (mode_slow){
	cur_vit = cur_vit/4;
      }
      vit += accel * direction.dot(current_direction);
      pos -= cur_vit * current_direction;
    }
  }
  //ofLogNotice() << i_target << endl;
  loadImagesDyn();
  
}

void BdGravitaire::draw(){
  ofPushMatrix();
  ofTranslate(w_mask, 0);
  ofScale(scale, scale);
  ofTranslate(pos.x, pos.y);
  //img.drawSubsection(0, 0, ofGetScreenWidth(), ofGetScreenHeight(), pos.x, pos.y, source_width, source_height);
  for (size_t i = 0; i<imgs.size(); i++){
    if (imgs[i].is_loaded){
      imgs[i].img.draw(imgs[i].pos_x,imgs[i].pos_y);
    }
  }
  ofPopMatrix();

  ofPushStyle();
  ofFill();
  ofSetColor(0,0,0);
  ofDrawRectangle(0,0, w_mask, screen_height);
  ofDrawRectangle(screen_width - w_mask, 0, w_mask, screen_height); 
  ofPopStyle();

  ofPushStyle();

  if (idle_out){
    float idle_time = ofGetElapsedTimef() - idle_out_start;
    if (idle_time < idle_out_duration){
      black_screen = ofMap(idle_time, 0, idle_out_duration, 0, 255);
    }else{
      idle_out = false;
      black_screen = 255;
    }
    
  }
  ofSetColor(0,0,0, black_screen);
  ofDrawRectangle(0, 0, screen_width, screen_height);
  ofPopStyle();

  if (mode_debug){
    ofPushStyle();
    ofSetColor(255,0,0);
    ofDrawBitmapString("gx: " +  ofToString(g_x) + " gy: " + ofToString(g_y), 20, 30); 
    ofDrawBitmapString("x: " +  ofToString(pos.x) + " y: " + ofToString(pos.y), 20, 50); 
    ofDrawBitmapString("dx: " +  ofToString(current_direction.x) + " dy: " + ofToString(current_direction.y), 20, 70); 
    ofDrawBitmapString("framerate: " + ofToString(ofGetFrameRate()), 20, 90); 
    ofDrawBitmapString("is_moving_away: " + ofToString(is_moving_away), 20, 110); 
    ofDrawBitmapString("data_path: " + ofToDataPath("toto "), 20, 130); 
    ofPopStyle();
  }

  drawBubble(ofVec2f(screen_width-(bubble_size+bubble_margin), screen_height-(bubble_size+bubble_margin)));
  if (show_map || map_anim_step != 0){
    drawMap();
  }
}

void BdGravitaire::drawBubble(ofPoint pos){
  ofPushMatrix();
  ofTranslate(pos.x, pos.y);

  
  ofPushStyle();

  ofNoFill();

  ofSetLineWidth(1);

  ofSetColor(128);
  ofDrawCircle(0, 0, 7);

  ofDrawLine(0, -bubble_size, 0, bubble_size);
  ofDrawLine(-bubble_size, 0, bubble_size, 0);
  
  
  ofSetLineWidth(3);
  ofSetColor(254);
  
  ofDrawCircle(0, 0, bubble_size-bubble_margin);

  ofSetColor(254);
  ofDrawCircle(ofMap(g_x, -1, 1, -bubble_size, bubble_size), 
	       ofMap(g_y, -1, 1, -bubble_size, bubble_size), 5);
  ofPopStyle();
  
  ofPopMatrix();
}

void BdGravitaire::drawMap(){
  ofPushMatrix();

  int pos_map ;

  if (show_map){
    if (map_anim_step < map_anim_nsteps){
      map_anim_step += 1;
    }
  }else{
    if (map_anim_step > 0){
      map_anim_step -= 1;
    }
  }
  ofTranslate(-screen_width+screen_width*map_anim_step/map_anim_nsteps, 0);

  ofPushStyle();
  ofSetColor(0,0,0,200);
  ofDrawRectangle(0, 0, screen_width, screen_height);

  ofPopStyle();
  
  ofPushMatrix();
  ofPushStyle();
  ofFill();
  
  float map_w_ratio = map_width/map_ratio;
  float map_h_ratio = map_height/map_ratio;
  

  
  ofTranslate((screen_width-map_w_ratio)/2, (screen_height-map_h_ratio)/2);
  ofScale(1/map_ratio, 1/map_ratio);
  
  ofTranslate(source_width/2, source_height/2);
  
  ofSetColor(200);
  
  ofPolyline lines;
  
  float line_width = source_height*0.9;
  
  ofSetLineWidth(0);
  
  
  //  ofSetLineWidth(source_height*2/map_ratio);
  ofDrawCircle(points[0].x, points[0].y, line_width/2);
  //  for (int i = 0; i < points.size()-1; i++){
  for (int i = 0; i < i_last_source; i++){
    ofDrawCircle(points[i+1].x, points[i+1].y, line_width/2);
    ofPoint pt1 = points[i];
    ofPoint pt2 = points[i+1];
    ofPushMatrix();
    ofTranslate(pt1);
    if (pt2.y < pt1.y){
      ofRotate(-(pt2-pt1).angle(ofPoint(1, 0)));
    }else{
      ofRotate((pt2-pt1).angle(ofPoint(1, 0)));
    }
    
    //ofRotate(ofPoint(1, 0).angle(ofPo));
    ofDrawRectangle(0, -line_width/2, pt1.distance(pt2), line_width);
    ofPopMatrix();
    //    lines.addVertex(ofPoint((points[i].x+source_width/2), (points[i].y+source_height/2)));
  }
  
  ofPoint pt1 = points[i_source];
  ofPoint pt2 = -pos;
  ofPushMatrix();
  ofTranslate(pt1);
  if (pt2.y < pt1.y){
    ofRotate(-(pt2-pt1).angle(ofPoint(1, 0)));
  }else{
    ofRotate((pt2-pt1).angle(ofPoint(1, 0)));
  }
  
  //ofRotate(ofPoint(1, 0).angle(ofPo));
  ofDrawRectangle(0, -line_width/2, pt1.distance(pt2), line_width);
  ofPopMatrix();
  
  ofSetColor(255);
  ofDrawCircle(-pos.x, -pos.y, line_width/2);
  
  lines.draw();
  ofPopStyle();
  
  // ofPushStyle();
  // ofSetColor(255, 0, 0);
  // ofDrawCircle((ofGetMouseX() - (screen_width-map_w_ratio)/2.0)*map_ratio  - source_width/2.0,
  // 	       (ofGetMouseY()  - (screen_height-map_h_ratio)/2.0)*map_ratio - source_height/2.0,
  // 	       5*map_ratio);
  // ofPopStyle();

  cout << i_last_source << endl;

  ofPopMatrix();

  ofPopMatrix();

  ofPopMatrix();
}

void BdGravitaire::mousePressedOnMap(){
  float mX = (ofGetMouseX() - (screen_width-map_w_ratio)/2.0)*map_ratio  - source_width/2.0;
  float mY = (ofGetMouseY() - (screen_height-map_h_ratio)/2.0)*map_ratio - source_height/2.0;

  ofPoint pos_m = ofPoint(mX, mY);
  for (int i = 0; i < i_last_source + 1; i++){
    if (pos_m.distance(points[i]) < source_width/2.0){
      cout << i << " " << pos_m.distance(points[i]) << "p" ;
      next_i_pos = i;
      idle_out = true;
      idle_out_start = ofGetElapsedTimef();
      show_map = false;
    }
  }

  cout << endl;
  
}

void BdGravitaire::showMap(){
  if (!show_map){
    map_anim_step = 0;
    show_map = true;
  }else{
    show_map = false;
  }
}

BdGravitaire::~BdGravitaire(){
  
}
