/**
 * Livre Gravitaire
 * 
 * @author Laurent Malys (lo@crans.org)
 * @version 0.3.0 29/05/2018
 */

#pragma once

#include "ofMain.h"
#include "ofxAndroid.h"
#include "ofxAccelerometer.h"
#include "bdGravitaire.h"

class ofApp : public ofxAndroidApp{
	
 public:
  
  void setup();
  void update();
  void draw();
  
  void keyPressed(int key);
  void keyReleased(int key);
  void windowResized(int w, int h);

  void touchDown(int x, int y, int id);
  void touchMoved(int x, int y, int id);
  void touchUp(int x, int y, int id);
  void touchDoubleTap(int x, int y, int id);
  void touchCancelled(int x, int y, int id);
  void swipe(ofxAndroidSwipeDir swipeDir, int id);
  
  void pause();
  void stop();
  void resume();
  void reloadTextures();
  
  bool backPressed();
  void okPressed();
  void cancelPressed();
  
 protected:
  BdGravitaire bd;
  float screen_width;
  float screen_height;
  bool b_update;

  bool draw_accel;

  ofVec3f accel, normAccel;
  string messages[3];

  ofImage img_test;

};
