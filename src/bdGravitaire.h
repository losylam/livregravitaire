/**
 * Livre Gravitaire
 * 
 * @author Laurent Malys (lo@crans.org)
 * @version 0.3.0 29/05/2018
 */


#ifndef SIMPLE_GRAVITAIRE_H
#define SIMPLE_GRAVITAIRE_H

#include "ofMain.h"
#include "ofxThreadedImageLoader.h"

class BdImage{
 public:
  string path;
  int pos_x;
  int pos_y;
  int num;
  ofImage img;
  bool is_loaded;
};

class BdGravitaire
{
 public:
  BdGravitaire();
  virtual ~BdGravitaire();
  
  void setup();
  void update(float x, float y);
  void updatePos();
  void draw();

  void slow(bool is_slow);
  
  void drawBubble(ofPoint pos);
  void drawMap();

  void showMap();
  
  void mousePressedOnMap();
  
  void loadImagesDyn();
 
  int isOnPoint();
  void moveForward(ofVec2f direction, int i);
  void moveBackward(ofVec2f direction, int i);

  ofxThreadedImageLoader loader;
  ofImage img;
  vector<BdImage> imgs;

  int screen_height;
  int screen_width;

  int source_width;
  int source_height;

  int map_width;
  int map_height;

  float map_w_ratio;
  float map_h_ratio;
  
  float map_ratio;

  bool show_map;
  int map_anim_step;
  int map_anim_nsteps;

  
  int w_mask;

  bool mode_slow;
  
  float accel;

  int angle_backward;
  int angle_forward;

  float g_x;
  float g_y;

  float min_y;
  float max_y;
  float min_x;
  float max_x;
  
  bool mode_debug;

  int carto_width;
  float scale;
  ofPoint pos;
  int next_i_pos;
  vector<ofPoint> points;
  ofVec2f current_direction;
  int is_on_point;
  int i_source;
  int i_target;
  ofPoint source;
  ofPoint target;

  int i_last_source;
  
  ofVec2f rel_pos;
  int is_moving_away;

  float vit;

  bool idle;
  int black_screen;
  float idle_start;
  float idle_duration;

  float idle_out;
  float idle_out_start;
  float idle_out_duration;
  
  int bubble_size;
  int bubble_margin;
};

#endif
