#!/env/python

import xml.dom.minidom as xml
ratio = 5746./591.


def getXY(src):
    doc = xml.parse(src)
    rect = doc.getElementsByTagName("rect")
    cadres = {}
    for el in rect:
        el_id = el.getAttribute('id')
        if len(el_id) > 0 and el_id[0] == 'z':
            cadre = {} 
            cadre['id'] = el_id
            cadre['x'] = el.getAttribute('x')
            cadre['y'] = el.getAttribute('y')
            cadre['w'] = el.getAttribute('width')
            cadre['h'] = el.getAttribute('height')
            
            cadres[el_id] = cadre
    return cadres

def writeXY(cadres, out):
    txt = ''
    lst_cle = cadres.keys()
    lst_cle.sort()
    for cle in lst_cle:
        cadre = cadres[cle]
        txt += '  points.push_back(ofPoint(%s, %s)); // %s\n' %(float(cadre['x'])*ratio, float(cadre['y'])*ratio, cle)
        
    f = open(out, 'w')
    f.write(txt)
    f.close()
